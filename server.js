// express, mongo
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var db = require('./utils/DatabaseMethods')

// webpack
var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var config = require('./webpack.config.js');

// server
var PRODUCTION = process.env.NODE_ENV === 'production';
var app = express();
db.setupConnection();


// static assets + port
app.set('port', process.env.PORT || 8080);
app.use(bodyParser.json());
app.use('/public', express.static(path.join(__dirname, 'public')));


// webpack middleware
if (!PRODUCTION) {

    var compiler = webpack(config);
    app.use(webpackDevMiddleware(compiler, {
        publicPath: '/public',
        index: "./views/index.html",
        quiet: false,
        stats: {
            colors: true
        }
    }));
    app.use(webpackHotMiddleware(compiler, {
        log: console.log,
        path: '/__webpack_hmr'
    }));
}


// API
app.get('/questions', (request, response) => {
    db.findRandomQuestion().then(data => response.send(data));
});

app.post('/save', (request, response) => {
    db.createQuestion(request.body);
});

app.post('/savescores', (request, response) => {
    db.saveScores(request.body);
});


// Роут и запуск
app.get('/', (request, response) => {
    response.sendFile(__dirname + '/views/index.html');
});



app.listen(app.get('port'), () => {
    console.log('Express server listening on port' + app.get('port'));
});