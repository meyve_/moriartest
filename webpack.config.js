const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

// Среда разработки
const DEVELOPMENT = process.env.NODE_ENV === 'development';
const PRODUCTION = process.env.NODE_ENV === 'production';

// Точки входа
const entry = PRODUCTION
    ? ['./app/main.js']
    : ['webpack/hot/dev-server', 'webpack-hot-middleware/client', './app/main.js'];

// Плагины
const plugins = PRODUCTION
    ? [
        new webpack
            .optimize
            .UglifyJsPlugin({
            sourceMap: false,
            comments: false,
            mangle: true,
            compress: {
                warnings: false
            }
        }),
        new ExtractTextPlugin({filename: 'css/styles.css', allChunks: true})
    ]
    : [new webpack.HotModuleReplacementPlugin()];

// Плагин в обе среды
if (DEVELOPMENT) {
    plugins.push(new webpack.DefinePlugin({
        DEVELOPMENT: JSON.stringify(DEVELOPMENT),
    }));
} else {
    plugins.push(new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify('production')
        }
    }));
}

// css-loader
const cssLoader = PRODUCTION
    ? {
        // PROD css-loader
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
                {
                    loader: 'css-loader'
                }, {
                    loader: 'postcss-loader'
                }, {
                    loader: 'sass-loader'
                }
            ],
            publicPath: '../'
        }),
        exclude: '/node_modules/'
    }
    : {
        // DEV css-loader
        test: /\.scss$/,
        use: [
            {
                loader: 'style-loader'
            }, {
                loader: 'css-loader',
                options: {
                    sourceMap: true
                }
            }, {
                loader: 'postcss-loader'
            }, {
                loader: 'sass-loader',
                options: {
                    sourceMap: true
                }
            }
        ]
    };

// Config
module.exports = {
    devtool: PRODUCTION
        ? ''
        : 'source-map',
    entry: entry,
    plugins: plugins,
    module: {
        rules: [
            {
                //JS лоадер
                test: /\.(js|jsx)$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['react', 'es2015', 'stage-0']
                        }
                    }
                ],
                exclude: '/node_modules/'
            }, {
                // Url лоадер
                test: /\.(png|jpg|jpeg|svg|gif|webm|mp4|mp3|ttf)$/,
                use: ['url-loader?limit=4096&name=imgs/[name].[ext]'],
                exclude: '/node_modules/'
            },
            cssLoader
        ]
    },
    output: {
        path: path.join(__dirname, 'public'),
        publicPath: '/public/',
        filename: 'js/bundle.js'
    }
};
