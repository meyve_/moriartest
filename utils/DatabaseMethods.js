var mongoose = require('mongoose');

// Модель и схема для вопроса
var questionSchema = new mongoose.Schema({
    question: String,
    answer: Number
});
var questionModel = mongoose.model('Questions', questionSchema);


// Модель и схема для юзера
var userSchema = new mongoose.Schema({
    username: String,
    scores: Number
});
var userModel = mongoose.model('Users', userSchema);


// Коннект к ДБ.
function setupConnection() {
    mongoose.connect('mongodb://admin:Bkxxqg11@ds145220.mlab.com:45220/didyoumissme');
    mongoose.connection.once('open', function () {
        console.log('Connected to DB.');
    }).on('error', function (error) {
        console.log(`Connection error: ${error}`);
    });
}


// Создать вопрос (для заполнения ДБ, потом удалю)
function createQuestion(data) {
    const newQuestion = new questionModel({
        question: data.question,
        answer: data.answer
    });

    return newQuestion.save();
}


// Сохранить результаты игры пользователя
function saveScores(data) {
    const newUser = new userModel({
        username: data.username,
        scores: data.scores
    });

    return newUser.save();
}


// Найти все вопросы и отправить в Action
function findRandomQuestion() {
    return questionModel.find();
}

module.exports.setupConnection = setupConnection;
module.exports.createQuestion = createQuestion;
module.exports.saveScores = saveScores;
module.exports.findRandomQuestion = findRandomQuestion;
