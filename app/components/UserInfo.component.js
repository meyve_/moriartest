import React from 'react';
import heart from '../assets/heart.png';


export default class UserInfo extends React.Component {
    constructor(props) {
        super(props);

    }


    render() {

        return (
            <div className="userinfo">
                <div className="userinfo_lives">
                    {this.props.lives.map((_, i) => {
                        return (<span key={i}>
                                    <img src={heart}/>
                                </span>);
                    })}
                </div>
                <div className="userinfo_scores">
                    <span>{this.props.username}:</span>
                    <span>{this.props.scores}</span>
                </div>
            </div>
        );
    }
};