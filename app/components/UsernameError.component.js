import React from 'react';


export default class UsernameError extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div className="username-error">Имя должно содержать от 1 до 12 букв латинского алфавита или цифр.</div>
        );
    }
};