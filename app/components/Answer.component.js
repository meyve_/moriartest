import React from 'react';


export default class Answer extends React.Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleSubmit(event) {
        event.preventDefault();
        this.props.answerSubmitHandler(this.answerInput.value);

        if (localStorage.lives > 0) {
            setTimeout(() => {
                this.answerInput.value = '';
            }, 50);
        }
    }

    render() {

        if (this.props.gameover) return null;

        return (
            <form className="answer" onSubmit={this.handleSubmit}>
                <input type="text" placeholder="Ответ" ref={input => this.answerInput = input}/>
                <button onClick={this.handleSubmit}>ОК</button>
            </form>
        );
    }
}