import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import {CSSTransitionGroup} from 'react-transition-group';

// flux stores, actions
import AppStore from '../stores/AppStore';
import * as AppActions from '../actions/AppActions';

// components
import Gameover from './Gameover.component';
import Timer from './Timer.component';
import Question from './Question.component';
import Answer from './Answer.component';
import UserInfo from './UserInfo.component';
import video from '../assets/tick_tock.webm';
import questionArray from '../../questionsArray';


export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            question: AppStore.randomQuestion.question,
            answer: AppStore.randomQuestion.answer,
            lives: +localStorage.lives,
            scores: +localStorage.scores
        };

        // this.gameover - для отображения компонента Gameover.
        // this.timer - константа, которую получает Timer в качестве изначального значения.
        this.gameover = false;
        this.timer = 59;


        // Биндинги
        this.updateQuestion = this.updateQuestion.bind(this);
        this.answerSubmitHandler = this.answerSubmitHandler.bind(this);
        this.setLivesArray = this.setLivesArray.bind(this);
        this.startAgain = this.startAgain.bind(this);
        this.timerHandler = this.timerHandler.bind(this);
        this.save = this.save.bind(this);
    }


    // Привязка к изменению AppStore и получение рандомного дефолтного вопроса.
    // window.usedQuestion - массив с _id уже использованных вопросов (см. AppActions).
    componentWillMount() {
        AppStore.on('change', this.updateQuestion);

        window.usedQuestions = [];
        this.setLivesArray();
        AppActions.getRandomQuestionFromDatabase();
    }

    componentWillUnmount() {
        AppStore.removeListener('change', this.updateQuestion);
    }


    // onchange callback
    updateQuestion() {
        this.setState(AppStore.updateView());
    }


    save() {
        AppActions.saveQuestionInDatabase(questionArray);
    }


    // Проверить значение ответа, если верно => новый вопрос, если нет - минус 1 жизнь.
    answerSubmitHandler(value) {
        let answer = this.state.answer;
        let userAnswer = value;


        // Полиморфизм -> строки\числа
        if (typeof(value) == String) {
            answer = String(answer).toLowerCase();
            userAnswer = String(userAnswer).toLowerCase();
        } else {
            answer = parseInt(answer);
            userAnswer = parseInt(userAnswer);
        }


        // Проверка ответа.
        if (answer === userAnswer) {
            localStorage.scores = parseInt(localStorage.scores) + 10;
            AppActions.getRandomQuestionFromDatabase();
        } else {
            // Вызов с 1 аргументом отнимает 1 жизнь (см. AppActions).
            // Также удаляем последний элемент livesArr - для корректного отображения сердечек жизней (см. UserInfo).
            this.livesArr.pop();
            AppActions.getRandomQuestionFromDatabase(localStorage.lives);
        }
    }


    // Создание массива для корректного отображения сердечек жизней.
    setLivesArray() {
        this.livesArr = [];
        for (let i = 0; i < localStorage.lives; i++) {
            this.livesArr.push(i);
        }
    }


    // Начать новую игру, не выходя в меню.
    startAgain() {
        // Обнуляем очки и жизни.
        localStorage.scores = 0;
        localStorage.lives = 3;

        // Убираем компонент GameOver.
        this.gameover = false;

        // Обновляем содержимое this.livesArr для корректного отображения сердечек жизней.
        this.setLivesArray();

        // Обнуляем window.usedQuestions и берем новый вопрос из БД.
        window.usedQuestions = [];
        AppActions.getRandomQuestionFromDatabase();
    }


    // Сброс таймера.
    timerHandler() {
        this.answerSubmitHandler('');
    }



    render() {

        // Если нет username и пользователь захотел включить игру через поисковую строку - редирект в меню.
        // Если жизни NaN т.е. пользователь нажал кнопку "ответ" при 0 жизней - редирект в меню.
        if (!localStorage.username || isNaN(localStorage.lives)) return <Redirect to="/"/>;

        // Если жизней 0 - сохраняем очки и отображаем компонент Gameover
        if (this.state.lives === 0) {

            AppActions.saveUserScoreWhenGameIsOver({
                username: localStorage.username,
                scores: +localStorage.scores || +0
            });

            delete localStorage.lives;
            this.gameover = true;
        }

        if (window.usedQuestions.length === window.questionCap) return <div>Больше нет вопросов хи-хи. Начните игру
            сначала.</div>;


        return (
            <CSSTransitionGroup transitionName="app-transition"
                                transitionAppear={true}
                                transitionAppearTimeout={500}
                                transitionEnter={false}
                                transitionLeave={false}>

                {/*Видео*/}
                <div id="video-wrapper">
                    <video autoPlay loop>
                        <source src={video} type="video/webm"/>
                    </video>
                </div>

                {/*Юзеринфо*/}
                <UserInfo username={localStorage.username} lives={this.livesArr} scores={this.state.scores}/>


                {/*Ссылка на главную*/}
                <div id="link-wrapper">
                    <Link className="link_home" to="/">
                        <span>
                            <svg viewBox="0 0 50 80">
                                <polyline className="polyline" points="45.63,75.8 0.375,38.087 45.63,0.375 "/>
                            </svg>
                        </span>
                    </Link>
                </div>


                {/*Аппка*/}
                <div className="app-wrapper">
                <Timer gameover={this.gameover} timer={this.timer} timerHandler={this.timerHandler}/>
                {/*<div className="timer"></div>*/}
                    {(this.gameover) ?
                        <Gameover startAgain={this.startAgain}/>
                        :
                        <Question data={this.state}/>
                    }
                    <Answer answerSubmitHandler={this.answerSubmitHandler} gameover={this.gameover}/>
                </div>

                {/*<button id='ss' onClick={this.save}>sss</button>*/}
            </CSSTransitionGroup>
        );
    }
}
