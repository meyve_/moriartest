import React from 'react';
import {CSSTransitionGroup} from 'react-transition-group';

// components
import Logo from './Logo.component';
import UsernameError from './UsernameError.component';
import audio from '../assets/opening.mp3';


export default class Menu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            usernameError: false,
            changeName: false
        };


        // Биндинги
        this.startNewGame = this.startNewGame.bind(this);
        this.continueGame = this.continueGame.bind(this);
        this.changeName = this.changeName.bind(this);
    }


    componentDidMount() {
        // Если уже прослушали музычку - больше не слушаем хи-хи
       if (!localStorage.playedMusic) {
           this.refs.audio.setAttribute('autoPlay', true);
           localStorage.playedMusic = true;
       } else {
           this.refs.audio.removeAttribute('autoPlay');
       }
    }

    // Новая игра.
    // В зависимости от наличия username в localStorage рендерит необходимые поля.
    startNewGame(event) {
        event.preventDefault();

        if (this.state.changeName || !localStorage.username) {
            const usernameValue = this.username.value;
            const regex = /^[\wа-яА-Я]{1,12}$/;

            // Проверка введенного имени.
            // Если все ок - запись в localStorage и редирект в игру.
            // Если нет, то отображается компонент ошибки UsernameError (см. render()).
            if (regex.test(usernameValue)) {

                localStorage.username = usernameValue;
                localStorage.scores = 0;
                localStorage.lives = 3;

                return this.props.history.push('/play');

            } else {
                this.setState({usernameError: true, changeName: true});
            }

            // Если username есть в localStorage -> редирект в игру с предварительным сбросом данных.
        } else {

            localStorage.scores = 0;
            localStorage.lives = 3;

            return this.props.history.push('/play');
        }
    }


    continueGame() {
        return this.props.history.push('/play');
    }

    changeName() {
        this.setState({
            userNameError: false,
            changeName: true
        });
    };


    render() {


        return (
            <CSSTransitionGroup transitionName="menu-transition"
                                transitionAppear={true}
                                transitionAppearTimeout={500}
                                transitionEnter={false}
                                transitionLeave={false}>
                <div className="menu-wrapper">
                    <div>
                        <audio ref="audio">
                            <source src={audio}/>
                        </audio>
                    </div>
                    <div className="img-wrapper">
                        <Logo/>
                    </div>


                    <div className="buttons-wrapper">
                        <div className="buttons-wrapper_inner">


                            {/*Отображение ошибки если неверно введено имя*/}
                            {this.state.usernameError ?
                                <UsernameError/>
                                :
                                null
                            }


                            {/* Если пользователь захотел сменить имя или в localstorage нет username - отображает поле для ввода имени*/}
                            {(this.state.changeName || !localStorage.username) ?
                                <form onSubmit={this.startNewGame}>
                                    {/*<label htmlFor="username-input">Ваше имя?</label>*/}
                                    <input type="text" placeholder="Ваше имя?" classID="username-input"
                                           ref={input => this.username = input}/>
                                    <button type="submit">СОХРАНИТЬ</button>
                                </form>
                                :
                                null
                            }


                            {/*Новая игра со сбросом данных*/}
                            <button onClick={this.startNewGame}>НАЧАТЬ ИГРУ</button>

                            {/*Отображает кнопку "Продолжить" если игра уже была начата */}
                            {(localStorage.lives > 0 && !this.state.changeName) ?
                                <button onClick={this.continueGame}>ПРОДОЛЖИТЬ</button>
                                :
                                null
                            }


                            {/*Смена имени -> имя записано в state*/}
                            <button onClick={this.changeName}>СМЕНИТЬ ИМЯ</button>
                            {/*<button>ТОП 10</button>*/}
                        </div>
                    </div>
                </div>
            </CSSTransitionGroup>
        );
    }
}

