import React from 'react';

export default class Timer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            timer: this.props.timer
        };
        this.timerID = null;

        this.resetTime = this.resetTime.bind(this);
    }


    componentDidMount() {
        this.timerID = setInterval(() => {
            this.setState({timer: this.state.timer - 1});
        }, 1000);
    }


    // Обновляем state когда получаем props.
    componentWillReceiveProps() {
        this.setState({
            timer: this.props.timer
        });
    }



    shouldComponentUpdate() {
        // Костыль для сброса интервала, когда сбрасывает таймер.
        if (this.state.timer === this.props.timer) {
            clearInterval(this.timerID);
            this.timerID = setInterval(() => {
                this.setState({timer: this.state.timer - 1});
            }, 1000);
            return true;

            // Если 0 жизней - апдейтим, и далее не рендерим.
        } else if (+localStorage.lives === 0) {
            clearInterval(this.timerID);
            return false;

            // Если таймер дошел до 0 - сброс.
        } else if (this.state.timer === 0) {
            this.props.timerHandler();
            return false;
        }
        return true;
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }



    // Сброс таймера на дефолтное значение - 59 секунд.
    resetTime() {
        this.props.timerHandler();
    }

    render() {

        // Если гейм-овер - не рендерить компонент.
        if (this.props.gameover) {
            clearInterval(this.timerID);
            return null;
        }

        return (
            <div className="timer">
                <span>{`0:${this.state.timer}`}</span>
            </div>
        );
    }
}