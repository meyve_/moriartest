import React from 'react';
import {CSSTransitionGroup} from 'react-transition-group';

export default class Question extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {

    }

    render() {
        return (
            <div className="question">
                <CSSTransitionGroup transitionName="question-transition"
                                    transitionEnter={true}
                                    transitionLeave={true}
                                    transitionEnterTimeout={1000}
                                    transitionLeaveTimeout={1000}>
                    <p key={this.props.data.question}>{this.props.data.question}</p>
                </CSSTransitionGroup>

                {/*<p>{this.props.data.answer}</p>*/}
            </div>
        );
    }
};