import React from 'react';


export default class Gameover extends React.Component {
    constructor(props) {
        super(props);

        this.startAgainHandler = this.startAgainHandler.bind(this);
    }


    startAgainHandler() {
        this.props.startAgain();
    }


    render() {

        return (
            <div className="gameover">
                <p>Игра окончена!</p>
                <button onClick={this.startAgainHandler}>Начать сначала</button>
            </div>
        );
    }
};