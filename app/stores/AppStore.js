import {EventEmitter} from 'events';
import Dispatcher from '../dispatcher';

class AppStore extends EventEmitter {
    constructor() {
        super();
        this.randomQuestion = this.setDefaultQuestion();


        // Биндинги
        this.setDefaultQuestion = this.setDefaultQuestion.bind(this);
        this.changeQuestion = this.changeQuestion.bind(this);
        this.updateView = this.updateView.bind(this);
        this.handleActions = this.handleActions.bind(this);
    }


    // Задать дефолтное состояние для view
    setDefaultQuestion() {
        return {
            question: ' ',
            answer: ' ',
            lives: +localStorage.lives,
            scores: +localStorage.scores
        }
    }


    // Получить рандомный вопрос и количество текущих жизней от AppActions
    // и триггернуть change эвент
    changeQuestion(data) {
        this.randomQuestion = {
            question: data.question,
            answer: data.answer,
            lives: data.lives,
            scores: data.scores
        };

        this.emit('change');
    }


    // Вернуть состояние на view при триггере change эвента
    updateView() {
        return this.randomQuestion;
    }


    // Управление входящими действиями
    handleActions(action) {
        switch (action.type) {
            case 'GET_RANDOM_QUESTION': {
                this.changeQuestion(action.data);
                break;
            }
        }
    }


}

const appStore = new AppStore;
Dispatcher.register(appStore.handleActions);
// window.Dispatcher = Dispatcher;
export default appStore;