import axios from 'axios';
import Dispatcher from '../dispatcher';


// Перемешка массива
Array.prototype.shuffle = function () {
    let currentIndex = this.length,
        temporaryValue,
        randomIndex;

    while (0 !== currentIndex) {

        // Выбрать рандомный элемент и уменьшить текущий на -1.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // Сменить текущий на рандомно выбранный и наоборот.
        temporaryValue = this[currentIndex];
        this[currentIndex] = this[randomIndex];
        this[randomIndex] = temporaryValue;
    }

    return this;
};


// Получение рандомного вопроса из БД.
// Если функция была вызвана с 1 аргументом (localStorage.lives) -> подсчитваем новое количество жизней.
export function getRandomQuestionFromDatabase() {
    let lives = arguments[0] || null;
    if (lives) {
        lives -= 1;
        localStorage.lives = lives;
    }

    axios.get('/questions')
        .then(response => {

            // Фильтрует массив из ответа API (response.data), чтобы в нем не было элементов из window.usedQuestions.
            // Потом перемешивает и берем первый элемент из нового (рандомо перемешанного) массива.
            // Пушит _id в массив window.usedQuestions и отправляет в AppStore.
            // _id - дефолтное свойство для каждого документа в mongoDB.
            // questionCap = общее количество доступных вопросов. (см App.js -> render());
            function findRandomQuestionFromResponse(response) {

                let filteredArray = response.data.filter((item) => {
                    return window.usedQuestions.indexOf(item._id) === -1;
                }).shuffle();


                let randomQuestion = filteredArray[0];
                window.usedQuestions.push(randomQuestion._id);
                window.questionCap = Number(response.data.length);


                // Багнутый поиск, запускает бесконечный цикл если заканчиваются вопросы.
                // Оставлю на память лул
                // do {
                //     randomQuestion = response.data[Math.floor(Math.random() * response.data.length)];
                //     console.log('RANDOM QUESTION:', randomQuestion);
                //     console.log('ARRAY', window.usedQuestions);
                // } while (window.usedQuestions.indexOf(randomQuestion._id) !== -1);

                return {
                    question: randomQuestion.question || 'уапросав больше нет',
                    answer: randomQuestion.answer,
                    lives: +localStorage.lives,
                    scores: +localStorage.scores
                };
            }

            // Flux-диспатч -> на AppStore
            Dispatcher.dispatch({
                type: 'GET_RANDOM_QUESTION',
                data: findRandomQuestionFromResponse(response)
            });
        })
        .catch(error => {
            console.error(error);
        });
}


// Сохранение результаты игры в БД после ее конца.
export function saveUserScoreWhenGameIsOver(data) {
    axios.post('/savescores', data)
        .then(response => {
            console.log(response);
        })
        .catch(error => {
            console.error(error);
        });
}

// Сохранение вопросов в ДБ. Поддерживает полиморфизм (массив\объект).
// Потом можно удалить.
export function saveQuestionInDatabase(data) {

    if (Array.isArray(data)) {
        data.map(question => {
            axios.post('/save', question)
                .then(response => {
                    console.log(response);
                })
                .catch(error => {
                    console.error(error);
                });
        });
    } else {
        axios.post('/save', data)
            .then(response => {
                console.log(response);
            })
            .catch(error => {
                console.error(error);
            });
    }
}

