import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

// components & styles
import App from './components/App';
import Menu from './components/Menu';
import styles from './stylesheets/styles.scss';


ReactDOM.render(
    <Router>
        <Switch>
            <Route exact path="/" component={Menu}/>
            <Route path="/play" component={App}/>

            {/*<Route component={NoMatch}/>*/}
        </Switch>
    </Router>,
    document.querySelector('#app')
);


// if (DEVELOPMENT) {
//     module.hot.accept();
// }


// TODO: !! поменять в модели вопроса number -> string/number.
// TODO: Сделать топ-10 игроков (?).
// TODO: тесты
